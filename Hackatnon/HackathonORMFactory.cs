﻿using Hackatnon.Code.ORM.Contexts;
using Hackatnon.Code.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon
{
	public class HackathonORMFactory
	{
		public static void EmptyDo()
		{
			SqlServerTypes.Utilities.LoadNativeAssemblies(AppDomain.CurrentDomain.BaseDirectory);
			var h = GetContext(null);
			return;
		}

		public static HackathonDBContext GetContext(IConfig config)
		{
			if (config == null)
			{
				return null;
			}

			var connectionString = config.GetConnectionString(HackathonDBContext.ConnectionStringName);
			return new HackathonDBContext(connectionString);

		}
	}
}