﻿using Hackatnon.Code.Domain.Account;
using Hackatnon.Code.Domain.Orders;
using Hackatnon.Code.Domain.Transport;
using Hackatnon.Code.ORM.ModelBuilders.Hackathon;
using MySql.Data.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.ORM.Contexts
{
	[DbConfigurationType(typeof(MySqlEFConfiguration))]
	public class HackathonDBContext: DbContext
	{
		public static string ConnectionStringName = "HackathonDatabase";

		public HackathonDBContext(string connectionString)
			: base(connectionString)
		{
			//this.Configuration.LazyLoadingEnabled = false;
			//this.Configuration.ProxyCreationEnabled = false;
			//bool instanceExists = System.Data.Entity.SqlServer.SqlProviderServices.Instance != null;
		}
		static HackathonDBContext()
		{

		}

		public DbSet<User> Users { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<CarShering> CarSherings { get; set; }
		public DbSet<Car> Cars { get; set; }
		public DbSet<OrderRequest> OrderRequests { get; set; }
		public DbSet<OrderRoutePoint> OrderRoutePoints { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			var userBuilder = new UserModelBuilder();
			userBuilder.Build(modelBuilder);


			var orderModelBuilder = new OrderModelBuilder();
			orderModelBuilder.Build(modelBuilder);

			var carSheringModelBuilder = new CarSheringModelBuilder();
			carSheringModelBuilder.Build(modelBuilder);

			var carModelBuilder = new CarModelBuilder();
			carModelBuilder.Build(modelBuilder);

			var orderRequestModelBuilder = new OrderRequestModelBuilder();
			orderRequestModelBuilder.Build(modelBuilder);

			var orderRoutePointModelBuilder = new OrderRoutePointModelBuilder();
			orderRoutePointModelBuilder.Build(modelBuilder);
		}
	}
}