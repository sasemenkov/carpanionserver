﻿using Hackatnon.Code.Domain.Account;
using Hackatnon.Code.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.ORM.ModelBuilders.Hackathon
{
	public class OrderModelBuilder
	{
		public void Build(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Order>().ToTable("hackathon.orders");
			modelBuilder.Entity<Order>()
				.Property(t => t.ID)
				.HasColumnName("ID");

			modelBuilder.Entity<Order>()
				.HasKey(b => b.ID);

			modelBuilder.Entity<Order>()
				.Property(t => t.ExID)
					.HasMaxLength(255)
					.HasColumnName("ExtID");
			modelBuilder.Entity<Order>()
			.Property(t => t.UserID)
				.HasMaxLength(36)
				.HasColumnName("UserID");
			modelBuilder.Entity<Order>()
			.Property(t => t.CarID)
				.HasMaxLength(36)
				.HasColumnName("CarID");
			modelBuilder.Entity<Order>()
				.Property(t => t.LatFrom)
					.HasColumnName("LatFrom");
			modelBuilder.Entity<Order>()
					.Property(t => t.LonFrom)
						.HasColumnName("LonFrom");

			modelBuilder.Entity<Order>()
				.Property(t => t.LatTo)
					.HasColumnName("LatTo");

			modelBuilder.Entity<Order>()
				.Property(t => t.LonTo)
					.HasColumnName("LonTo");
			modelBuilder.Entity<Order>()
				.Property(t => t.UserDriveStateInOrder)
					.HasColumnName("UserDriveStateInOrder");
			modelBuilder.Entity<Order>()
				.Property(t => t.StartDateRange)
					.HasColumnName("StartDateRange");
			modelBuilder.Entity<Order>()
				.Property(t => t.EndDateRange)
					.HasColumnName("EndDateRange");
			modelBuilder.Entity<Order>()
				.Property(t => t.WaitLimitMin)
					.HasColumnName("WaitLimitMin");
			modelBuilder.Entity<Order>()
				.Property(t => t.IsNeedCompanion)
					.HasColumnName("IsNeedCompanion");
		}
	}
}