﻿using Hackatnon.Code.Domain.Account;
using Hackatnon.Code.Domain.Orders;
using Hackatnon.Code.Domain.Transport;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.ORM.ModelBuilders.Hackathon
{
	public class OrderRequestModelBuilder
	{
		public void Build(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<OrderRequest>().ToTable("hackathon.order_requests");
			modelBuilder.Entity<OrderRequest>()
				.Property(t => t.ID)
				.HasColumnName("ID");

			modelBuilder.Entity<OrderRequest>()
				.HasKey(b => b.ID);

			
			modelBuilder.Entity<OrderRequest>()
				.Property(t => t.State)
					
					.HasColumnName("State");
			modelBuilder.Entity<OrderRequest>()
				.Property(t => t.OrderID)
				.HasMaxLength(36)
					.HasColumnName("OrderID");
			modelBuilder.Entity<OrderRequest>()
				.Property(t => t.SubOrderID)
				.HasMaxLength(36)
					.HasColumnName("SubOrderID");
			


		}
	}
}