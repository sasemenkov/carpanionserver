﻿using Hackatnon.Code.Domain.Account;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.ORM.ModelBuilders.Hackathon
{
	public class UserModelBuilder
	{
		public void Build(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<User>().ToTable("hackathon.users");
			modelBuilder.Entity<User>()
				.Property(t => t.ID)
				.HasColumnName("ID");

			modelBuilder.Entity<User>()
				.HasKey(b => b.ID);

			modelBuilder.Entity<User>()
				.Property(t => t.Login)
					.HasMaxLength(512)
					.HasColumnName("Login");
			modelBuilder.Entity<User>()
				.Property(t => t.ExtID)
					.HasMaxLength(255)
					.HasColumnName("ExtID");
			modelBuilder.Entity<User>()
					.Property(t => t.PhoneNumber)
						.HasMaxLength(32)
						.HasColumnName("PhoneNumber");

			modelBuilder.Entity<User>()
				.Property(t => t.HasCarLicense)
					.HasColumnName("HasCarLicense");
			modelBuilder.Entity<User>()
				.Property(t => t.Sex)
					.HasColumnName("Sex");

			modelBuilder.Entity<User>()
				.Property(t => t.ImageUrl)
					.HasMaxLength(1024)
					.HasColumnName("ImageUrl");
			modelBuilder.Entity<User>()
				.Property(t => t.FirstName)
					.HasMaxLength(256)
					.HasColumnName("FirstName");
			modelBuilder.Entity<User>()
				.Property(t => t.SecondName)
					.HasMaxLength(256)
					.HasColumnName("SecondName");
			
			modelBuilder.Entity<User>()
				.Property(t => t.EMail)
					.HasMaxLength(1024)
					.HasColumnName("EMail");
			modelBuilder.Entity<User>()
				.Property(t => t.Password)
					.HasMaxLength(512)
					.HasColumnName("Password");
		}
	}
}