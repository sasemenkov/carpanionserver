﻿using Hackatnon.Code.Domain.Account;
using Hackatnon.Code.Domain.Transport;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.ORM.ModelBuilders.Hackathon
{
	public class CarModelBuilder
	{
		public void Build(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Car>().ToTable("hackathon.cars");
			modelBuilder.Entity<Car>()
				.Property(t => t.ID)
				.HasColumnName("ID");

			modelBuilder.Entity<Car>()
				.HasKey(b => b.ID);

			modelBuilder.Entity<Car>()
				.Property(t => t.Lon)
					.HasColumnName("Lon");
			modelBuilder.Entity<Car>()
				 .Property(t => t.Lat)
					 .HasColumnName("Lat");

			modelBuilder.Entity<Car>()
				.Property(t => t.ExID)
					.HasMaxLength(255)
					.HasColumnName("ExID");
			modelBuilder.Entity<Car>()
				.Property(t => t.CarSheringExID)
					.HasMaxLength(255)
					.HasColumnName("CarSheringExID");
			modelBuilder.Entity<Car>()
					.Property(t => t.Name)
						.HasMaxLength(256)
						.HasColumnName("Name");

			modelBuilder.Entity<Car>()
				.Property(t => t.Number)
						.HasMaxLength(32)
					.HasColumnName("Number");
		

			modelBuilder.Entity<Car>()
				.Property(t => t.Fuel)
					.HasColumnName("Fuel");
			modelBuilder.Entity<Car>()
				.Property(t => t.UrlScheme)
					.HasMaxLength(1024)
					.HasColumnName("UrlScheme");
			
		}
	}
}