﻿using Hackatnon.Code.Domain.Account;
using Hackatnon.Code.Domain.Orders;
using Hackatnon.Code.Domain.Transport;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.ORM.ModelBuilders.Hackathon
{
	public class OrderRoutePointModelBuilder
	{
		public void Build(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<OrderRoutePoint>().ToTable("hackathon.order_route_points");
			modelBuilder.Entity<OrderRoutePoint>()
				.Property(t => t.ID)
				.HasColumnName("ID");

			modelBuilder.Entity<OrderRoutePoint>()
				.HasKey(b => b.ID);

			
			modelBuilder.Entity<OrderRoutePoint>()
				.Property(t => t.OrderID)
					.HasMaxLength(36)
					.HasColumnName("OrderID");
			modelBuilder.Entity<OrderRoutePoint>()
				.Property(t => t.Lat)
					.HasColumnName("Lat");
			modelBuilder.Entity<OrderRoutePoint>()
				.Property(t => t.Lon)
					.HasColumnName("Lon");
			modelBuilder.Entity<OrderRoutePoint>()
				.Property(t => t.Sort)
					.HasColumnName("Sort");


		}
	}
}