﻿using Hackatnon.Code.Domain.Account;
using Hackatnon.Code.Domain.Transport;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.ORM.ModelBuilders.Hackathon
{
	public class CarSheringModelBuilder
	{
		public void Build(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<CarShering>().ToTable("hackathon.car_sherings");
			modelBuilder.Entity<CarShering>()
				.Property(t => t.ID)
				.HasColumnName("ID");

			modelBuilder.Entity<CarShering>()
				.HasKey(b => b.ID);

			
			modelBuilder.Entity<CarShering>()
				.Property(t => t.ExID)
					.HasMaxLength(255)
					.HasColumnName("ExID");
			modelBuilder.Entity<CarShering>()
				.Property(t => t.Name)
					.HasMaxLength(255)
					.HasColumnName("Name");

			
		}
	}
}