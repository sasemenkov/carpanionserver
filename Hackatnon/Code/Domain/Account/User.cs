﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.Domain.Account
{
	public class User
	{
		public string ID { get; set; }
		public string Login { get; set; }
		public string ExtID { get; set; }
		public string PhoneNumber { get; set; }
		public bool HasCarLicense { get; set; }
		public string ImageUrl { get; set; }
		public string FirstName { get; set; }
		public string SecondName { get; set; }
		public string Patronymic { get; set; }
		public int Sex { get; set; }
		public string EMail { get; set; }
		public string Password { get; set; }


	}
}