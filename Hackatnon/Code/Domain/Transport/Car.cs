﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.Domain.Transport
{
	public class Car
	{
		public string ID { get; set; }
		public double Lon { get; set; }
		public double Lat { get; set; }
		public string ExID { get; set; }
		public string CarSheringExID { get; set; }
		public string Name { get; set; }
		public string Number { get; set; }
		public int Fuel { get; set; }
		public List<Tariff> Tariffs { get; set; }
		public string UrlScheme { get; set; }
		
	}
}