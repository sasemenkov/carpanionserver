﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.Domain.Orders
{
	public enum OrderRequestState
	{
		None = 0,
		Requested = 1,
		Approved = 2
		
	}
}