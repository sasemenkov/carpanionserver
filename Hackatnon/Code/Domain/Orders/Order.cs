﻿using Hackatnon.Code.Domain.GIS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.Domain.Orders
{
	public class Order
	{
		public string ID { get; set; }
		public string ExID { get; set; }
		public double LatFrom { get; set; }
		public double LonFrom { get; set; }
		public double LatTo { get; set; }
		public double LonTo { get; set; }
		public int UserDriveStateInOrder { get; set; }
		public DateTime StartDateRange { get; set; }
		public DateTime EndDateRange { get; set; }
		public int WaitLimitMin { get; set; }
		public bool IsNeedCompanion { get; set; }
		public string UserID { get; set; }
		public string CarID { get; set; }


	}
}