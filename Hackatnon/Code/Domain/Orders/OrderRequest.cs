﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.Domain.Orders
{
	public class OrderRequest
	{
		public string ID { get; set; }
		public string OrderID { get; set; }
		public string SubOrderID { get; set; }
		public int State { get; set; }
	}
}