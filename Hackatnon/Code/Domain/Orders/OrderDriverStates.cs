﻿

namespace Hackatnon.Code.Domain.Orders
{
	public enum OrderDriverStates
	{
		Driver = 0,
		Passenger = 1
	}
}