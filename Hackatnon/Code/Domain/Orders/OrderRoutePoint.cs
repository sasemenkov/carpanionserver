﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.Domain.Orders
{
	public class OrderRoutePoint
	{
		public string ID { get; set; }
		public string OrderID { get; set; }
		public double Lat { get; set; }
		public double Lon { get; set; }
		public int Sort { get; set; }
	}
}