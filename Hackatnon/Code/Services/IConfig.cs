﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackatnon.Code.Services
{
	public interface IConfig
	{
		string GetConnectionString(string name);
		string GetSetting(string name);
	}
}
