﻿using Hackatnon.Code.Domain.GIS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Hackatnon.Code.BL
{
	public class HereRepository
	{
		public string AppID = @"Z9MAK4aPGzB5hjBMkfQS";
		public string AppCode = @"7wBU7zq5boUyMj3xb9HRdw";
		public string RootHere = @"https://route.api.here.com/routing/7.2/calculateroute.json";
		//https://route.api.here.com/routing/7.2/calculateroute.json?
		//waypoint0=52.5160%2C13.3779&waypoint1=52.5206%2C13.3862
		//&mode=fastest%3Bcar%3Btraffic%3Aenabled&
		//app_id=devportal-demo-20180625&app_code=9v2BkviRwi9Ot26kp2IysQ
		//&departure=now

		public Route GetRoute(Coord point1, Coord point2)
		{
			var result = new Route();
			var wayPoint0 = string.Format("{0},{1}", point1.Lat.ToString().Replace(',', '.'), point1.Lon.ToString().Replace(',', '.'));
			var wayPoint1 = string.Format("{0},{1}", point2.Lat.ToString().Replace(',', '.'), point2.Lon.ToString().Replace(',', '.'));
			var ceilUrl = string.Format("{0}?mode=fastest;car;traffic:enabled&routeattributes=sh,gr&app_id={1}&app_code={2}&waypoint0={3}&waypoint1={4}", RootHere, AppID, AppCode,wayPoint0, wayPoint1);
			var body = Send(ceilUrl);
			if (!string.IsNullOrEmpty(body))
			{
				dynamic d = JsonConvert.DeserializeObject(body);
				dynamic leg = d.response.route[0].leg[0];
				dynamic start = leg.start;
				dynamic end = leg.end;
				dynamic maneuver = leg.maneuver;
				var coords = new List<Coord>();
				var startCoord = new Coord() { Lat = start.mappedPosition.latitude, Lon = start.mappedPosition.longitude };
				coords.Add(startCoord);
				var endCoord = new Coord() { Lat = end.mappedPosition.latitude, Lon = end.mappedPosition.longitude };
				foreach (var point in maneuver)
				{
					var coord = new Coord() { Lat = point.position.latitude, Lon = point.position.longitude };
					coords.Add(coord);
				}
				coords.Add(endCoord);
				result.Points = coords;
			}
			
			return result;
		}
		public string Send(string url)
		{
			var result = string.Empty;
			using (HttpClient client = new HttpClient())
			{
				// Call asynchronous network methods in a try/catch block to handle exceptions
				try
				{
				
					client.BaseAddress = new Uri(url);
					client.DefaultRequestHeaders.Accept.Clear();
					client.DefaultRequestHeaders.Accept.Add(
						new MediaTypeWithQualityHeaderValue("application/json"));
					
					
					//var content = data;
					HttpRequestMessage request = new HttpRequestMessage();
					HttpResponseMessage response = null;
					response = client.GetAsync(url).Result;

					if (response != null && response.IsSuccessStatusCode)
					{
						result = response.Content.ReadAsStringAsync().Result;
					}

					Console.WriteLine(result);
				}
				catch (HttpRequestException e)
				{
					Console.WriteLine("\nException Caught!");
					Console.WriteLine("Message :{0} ", e.Message);
				}
			}
			return result;
		}

	}
}