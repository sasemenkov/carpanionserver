﻿using Hackatnon.Code.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.BL
{
	public class HackathonWebConfig: IConfig
	{
		public string GetConnectionString(string name)
		{
			return ConfigurationManager.ConnectionStrings[name].ConnectionString;
		}
		public string GetSetting(string name)
		{

			return ConfigurationManager.AppSettings[name];
		}
	}
}