﻿using Hackatnon.Code.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.DTO.Order
{
	public class PostOrderRequestDTO:BaseDTO
	{
		public string ExID { get; set; }
		public double LatFrom { get; set; }
		public double LonFrom { get; set; }
		public double LatTo { get; set; }
		public double LonTo { get; set; }
		public int UserDriveStateInOrder { get; set; }
		public int StartDateRange { get; set; }
		public int EndDateRange { get; set; }
		public int WaitLimitMin { get; set; }
		public bool IsNeedCompanion { get; set; }
	}
}