﻿using Hackatnon.Code.Domain.GIS;
using Hackatnon.Code.Domain.Orders;
using Hackatnon.Code.Domain.Transport;
using Hackatnon.Code.DTO.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.DTO.Order
{
	public class OrderDTO
	{
		public OrderDTO(Hackatnon.Code.Domain.Orders.Order order)
		{
			ID = order.ID;
			ExID = order.ExID;
			LatFrom = order.LatFrom;
			LonFrom = order.LonFrom;
			LatTo = order.LatTo;
			LonTo = order.LonTo;
			UserDriveStateInOrder = order.UserDriveStateInOrder;
			DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			TimeSpan startTime = order.StartDateRange - Epoch;
			StartDateRange = (int)startTime.TotalSeconds;

			TimeSpan endTime = order.EndDateRange - Epoch;
			EndDateRange = (int)endTime.TotalSeconds;
			CarID = order.CarID;


			//EndDateRange = order.EndDateRange;
			WaitLimitMin = order.WaitLimitMin;
			IsNeedCompanion = order.IsNeedCompanion;
		}
		public string ID { get; set; }
		public string ExID { get; set; }
		public double LatFrom { get; set; }
		public double LonFrom { get; set; }
		public double LatTo { get; set; }
		public double LonTo { get; set; }
		public int UserDriveStateInOrder { get; set; }
		public int StartDateRange { get; set; }
		public int EndDateRange { get; set; }
		public int WaitLimitMin { get; set; }
		public bool IsNeedCompanion { get; set; }
		public string UserExID { get; set; }
		public string CarID { get; set; }
		public UserDTO User { get; set; }
		public List<OrderDTO> SubOrders { get; set; }
		public List<OrderDTO> Requesters { get; set; }
		public List<OrderDTO> Requests { get; set; }
		public List<OrderDTO> Drivers { get; set; }
		public bool IsOwner { get; set; }
		public List<Coord> Routes { get; set; }
		public Car Car { get; set; }
		

	}
}