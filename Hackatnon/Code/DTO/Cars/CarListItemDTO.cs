﻿using Hackatnon.Code.Domain.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.DTO.Cars
{
	public class CarListItemDTO
	{
		public CarListItemDTO(Car car)
		{
			ExID = car.ExID;
			Lat = car.Lat;
			Lon = car.Lon;
			CarSheringExID = car.CarSheringExID;
		}
		public string ExID { get; set; }
		public double Lat { get; set; }
		public double Lon { get; set; }
		public string CarSheringExID { get; set; }
	}
}