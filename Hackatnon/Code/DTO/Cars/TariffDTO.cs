﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.DTO.Cars
{
	public class TariffDTO
	{
		public string ExID { get; set; }
		public string Name { get; set; }
		public double Price { get; set; }
		public double Sale { get; set; }
	}
}