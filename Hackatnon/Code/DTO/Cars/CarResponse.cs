﻿using Hackatnon.Code.Domain.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.DTO.Cars
{
	public class CarResponse
	{
		public CarResponse(Car car)
		{
			ExID = car.ExID;
			Lat = car.Lat;
			Lon = car.Lon;
			CarSheringExID = car.CarSheringExID;
			Name = car.Name;
			Number = car.Number;
			Fuel = car.Fuel;
			//Tariffs  = car.Tariffs
			UrlScheme = car.UrlScheme;

		}
		public string ExID { get; set; }
		public double Lat { get; set; }
		public double Lon { get; set; }
		public string CarSheringExID { get; set; }
		public string Name { get; set; }
		public string Number { get; set; }
		public int Fuel { get; set; }
		public List<TariffDTO> Tariffs { get; set; }
		public string UrlScheme { get; set; }
	}
}