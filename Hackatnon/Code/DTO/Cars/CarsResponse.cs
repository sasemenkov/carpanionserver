﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.DTO.Cars
{
	public class CarsResponse
	{
		public List<CarListItemDTO> Cars { get; set; }
	}
}