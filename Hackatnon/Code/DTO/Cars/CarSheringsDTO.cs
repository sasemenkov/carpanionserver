﻿using Hackatnon.Code.Domain.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.DTO.Cars
{
	public class CarSheringDTO
	{
		public CarSheringDTO(CarShering carShering)
		{
			ExID = carShering.ExID;
			Name = carShering.Name;
		}
		public string ExID { get; set; }
		public string Name { get; set; }
	}
}