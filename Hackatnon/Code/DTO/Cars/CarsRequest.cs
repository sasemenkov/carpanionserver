﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.DTO.Cars
{
	public class CarsRequest: BaseDTO
	{
		public double Lat { get; set; }
		public double Lon { get; set; }
	
	}
}