﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.DTO.Routes
{
	public class GetDriversForOrderRequest:BaseDTO
	{
		public string OrderID { get; set; }
	}
}