﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.DTO.Routes
{
	public class ApproveOrderRequest:BaseDTO
	{
		public string OrderExID { get; set; }
		public string SubOrderExID { get; set; }
	}
}