﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.DTO.Routes
{
	public class RequestOrderRequest:BaseDTO
	{
		public string TargetOrderExID { get; set; }
		public string OwnOrderExID { get; set; }
	}
}