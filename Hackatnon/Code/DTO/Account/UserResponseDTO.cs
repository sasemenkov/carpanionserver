﻿using Hackatnon.Code.Domain.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.DTO.Account
{
	public class UserResponseDTO
	{
		public UserResponseDTO(User user)
		{
			ExtID = user.ExtID;
			PhoneNumber = user.PhoneNumber;
			HasCarLicense = user.HasCarLicense;
			ImageUrl = user.ImageUrl;
			FirstName = user.FirstName;
			SecondName = user.SecondName;
			EMail = user.EMail;



		}

		public string ExtID { get; set; }
		public string PhoneNumber { get; set; }
		public bool HasCarLicense { get; set; }
		public string ImageUrl { get; set; }
		public string FirstName { get; set; }
		public string SecondName { get; set; }
		public string EMail { get; set; }
	}
}