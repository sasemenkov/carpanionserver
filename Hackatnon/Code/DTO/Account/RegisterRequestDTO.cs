﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.DTO.Account
{
	public class RegisterRequestDTO
	{
		public string Login { get; set; }

		public string PhoneNumber { get; set; }
		public string FirstName { get; set; }
		public string SecondName { get; set; }
		public string Patronymic { get; set; }
		public int Sex { get; set; }
		public string EMail { get; set; }
		public string Password { get; set; }

	}
}