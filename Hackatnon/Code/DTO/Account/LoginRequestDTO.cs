﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackatnon.Code.DTO.Account
{
	public class LoginRequestDTO
	{
		public string Login { get; set; }
		public string Password { get; set; }
	}
}