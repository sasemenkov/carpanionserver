﻿using Hackatnon.Code.BL;
using Hackatnon.Code.Domain.Account;
using Hackatnon.Code.Domain.GIS;
using Hackatnon.Code.Domain.Orders;
using Hackatnon.Code.DTO;
using Hackatnon.Code.DTO.Account;
using Hackatnon.Code.DTO.Order;
using Hackatnon.Code.DTO.Routes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Hackatnon.Controllers
{
	public class OrderController : ApiController
	{
		[HttpGet]
		public HttpResponseMessage RequestOrder([FromUri]RequestOrderRequest request)
		{
			var ExtUserID = request.ExtUserID;
			var ownOrderExID = request.OwnOrderExID;
			var targetOrderExID = request.TargetOrderExID;
			var config = new HackathonWebConfig();
			using (var contextDB = HackathonORMFactory.GetContext(config))
			{
				//Check User
				var currentUser = contextDB.Users.Where(u => u.ExtID == ExtUserID).FirstOrDefault();
				if (currentUser != null)
				{
					var targetOrder = contextDB.Orders.Where(o => o.ExID==request.TargetOrderExID).FirstOrDefault();
					var ownOrder = contextDB.Orders.Where(o => o.ExID == request.OwnOrderExID).FirstOrDefault();
					if (targetOrder != null && ownOrder != null)
					{
						var newRequestOrder = new OrderRequest()
						{

							ID = Guid.NewGuid().ToString(),
							SubOrderID = ownOrder.ExID,
							OrderID = targetOrder.ExID,
							State = 1
						};
						contextDB.OrderRequests.Add(newRequestOrder);
						contextDB.ChangeTracker.DetectChanges();
						contextDB.SaveChanges();
						return Request.CreateResponse(
							HttpStatusCode.Created,
							true,
							Configuration.Formatters.JsonFormatter);
					}
					else
					{
						return Request.CreateResponse(
							HttpStatusCode.NotFound,
							false,
							Configuration.Formatters.JsonFormatter);
					}
					
				}
				else
				{
					var message = "Такого пользователя нет, но он не зарегестрирован";
					return Request.CreateResponse(
						HttpStatusCode.NotFound,
						message,
						Configuration.Formatters.JsonFormatter);
				}
			}
		}
		[HttpGet]
		public HttpResponseMessage ApproveOrder([FromUri]ApproveOrderRequest request)
		{
			var ExtUserID = request.ExtUserID;
			var orderExID = request.OrderExID;
			var subOrderExID = request.SubOrderExID;
			var config = new HackathonWebConfig();
			using (var contextDB = HackathonORMFactory.GetContext(config))
			{
				//Check User
				var currentUser = contextDB.Users.Where(u => u.ExtID == ExtUserID).FirstOrDefault();
				if (currentUser != null)
				{
					var order = contextDB.Orders.Where(o => o.ExID == orderExID).FirstOrDefault();
					var subOrder = contextDB.Orders.Where(o => o.ExID == subOrderExID).FirstOrDefault();
					if (order != null && subOrder != null)
					{
						var ceilRequest = contextDB.OrderRequests.Where(or => or.State == 1 && or.OrderID == order.ID && or.SubOrderID == subOrder.ExID).FirstOrDefault();
						if (ceilRequest != null)
						{
							ceilRequest.State = 2;
							contextDB.Entry(ceilRequest).State = EntityState.Modified;
							contextDB.ChangeTracker.DetectChanges();
							contextDB.SaveChanges();
							return Request.CreateResponse(
								HttpStatusCode.OK,
								true,
								Configuration.Formatters.JsonFormatter);
						}
						else
						{
							return Request.CreateResponse(
							   HttpStatusCode.NotFound,
							   false,
							   Configuration.Formatters.JsonFormatter);
						}
					}
					else
					{
						return Request.CreateResponse(
							HttpStatusCode.NotFound,
							false,
							Configuration.Formatters.JsonFormatter);
					}

				}
				else
				{
					var message = "Такого пользователя нет, но он не зарегестрирован";
					return Request.CreateResponse(
						HttpStatusCode.NotFound,
						message,
						Configuration.Formatters.JsonFormatter);
				}
			}
		}
		[HttpGet]
		public HttpResponseMessage ChooseAuto([FromUri]ChooseAutoRequest request)
		{
			var ExtUserID = request.ExtUserID;
			var orderExID = request.OrderExID;
			var carExID = request.CarExID;
			var config = new HackathonWebConfig();
			using (var contextDB = HackathonORMFactory.GetContext(config))
			{
				//Check User
				var currentUser = contextDB.Users.Where(u => u.ExtID == ExtUserID).FirstOrDefault();
				if (currentUser != null)
				{
					var order = contextDB.Orders.Where(o => o.ExID == orderExID && o.UserID==currentUser.ID).FirstOrDefault();
					var car = contextDB.Cars.Where(o => o.ExID == carExID).FirstOrDefault();
					if (order != null && car != null)
					{
						order.CarID = car.ID;
						contextDB.Entry(order).State = EntityState.Modified;
						contextDB.ChangeTracker.DetectChanges();
						contextDB.SaveChanges();
						return Request.CreateResponse(
							HttpStatusCode.OK,
							true,
							Configuration.Formatters.JsonFormatter);
					}
					else
					{
						return Request.CreateResponse(
							HttpStatusCode.NotFound,
							false,
							Configuration.Formatters.JsonFormatter);
					}

				}
				else
				{
					var message = "Такого пользователя нет, но он не зарегестрирован";
					return Request.CreateResponse(
						HttpStatusCode.NotFound,
						message,
						Configuration.Formatters.JsonFormatter);
				}
			}
		}
		//
		[HttpGet]
		public HttpResponseMessage GetDrivers([FromUri]GetDriversRequestDTO request)
		{
			var ExtUserID = request.ExtUserID;

			var config = new HackathonWebConfig();
			using (var contextDB = HackathonORMFactory.GetContext(config))
			{
				//Check User
				var currentUser = contextDB.Users.Where(u => u.ExtID == ExtUserID).FirstOrDefault();
				if (currentUser!=null)
				{

					var orders = contextDB.Orders.Where(o=>o.UserID != currentUser.ID && o.UserDriveStateInOrder == (int)OrderDriverStates.Driver && o.ExID!=request.ExID).ToList();
					//++фильтрация по маршруту
					if (orders != null && orders.Any())
					{
						var response = new OrderListResponceDTO();
						response.Orders = new List<OrderDTO>();
						foreach (var order in orders)
						{
							var orderDTO = new OrderDTO(order);
							var orderUser = contextDB.Users.FirstOrDefault(u => u.ID == order.UserID);
							if (orderUser != null)
							{
								orderDTO.IsOwner = orderUser.ExtID == ExtUserID;
								//orderDTO.User = new UserDTO(orderUser);
							}
							response.Orders.Add(orderDTO);
						}

						return Request.CreateResponse(
							HttpStatusCode.OK,
							response,
							Configuration.Formatters.JsonFormatter);
					}
					else
					{
						var message = "Заявок не найдено";
						return Request.CreateResponse(
							HttpStatusCode.NotFound,
							message,
							Configuration.Formatters.JsonFormatter);
					}
				}
				else
				{
					var message = "Такого пользователя нет, но он не зарегестрирован";
					return Request.CreateResponse(
						HttpStatusCode.NotFound,
						message,
						Configuration.Formatters.JsonFormatter);
				}
			}
		}
		// GET api/values/5
		//получить ордер по id ордера
		//заполнить для водителя - запросами поездки и одобренными, а также машиной, если есть
		//заполнить для пассажира - Родительским ордером и машиной если есть
		[HttpGet]
		public HttpResponseMessage Get([FromUri]OrderRequestDTO request)
		{
			var ExtUserID = request.ExtUserID;
			var orderExID = request.ExID;
			var config = new HackathonWebConfig();
			using (var contextDB = HackathonORMFactory.GetContext(config))
			{
				//Check User
				var currentUser = contextDB.Users.Where(u => u.ExtID == ExtUserID).FirstOrDefault();
				if (currentUser!=null)
				{

					var order = contextDB.Orders.Where(o => o.ExID == orderExID).FirstOrDefault();

					if (order != null)
					{
						var response = new OrderDTO(order);
						response.IsOwner = order.ExID == ExtUserID;
						if (order.UserDriveStateInOrder == (int)OrderDriverStates.Driver)
						{
							var routes = contextDB.OrderRoutePoints.Where(op => op.OrderID == order.ID).Select(op => new Coord() { Lat = op.Lat, Lon = op.Lon }).ToList();
							response.Routes = routes;
							if (!string.IsNullOrEmpty(order.CarID))
							{
								response.Car = contextDB.Cars.Where(c => c.ID == order.CarID).FirstOrDefault();
							}
							//Список запросов
							var orderRequests = contextDB.OrderRequests.Where(or => or.OrderID == order.ID && or.State == (int)OrderRequestState.Requested).Select(p => p.SubOrderID).ToList();
							if (orderRequests != null && orderRequests.Any())
							{
								var subOrders = contextDB.Orders.Where(p => orderRequests.Contains(p.ID)).ToList();
								response.Requesters = subOrders.Select(p => new OrderDTO(p)).ToList();
							}
							//Список подтвержденных
							var ordersApproved = contextDB.OrderRequests.Where(or => or.OrderID == order.ID && or.State == (int)OrderRequestState.Approved).Select(p=>p.SubOrderID).ToList();
							if (ordersApproved != null && ordersApproved.Any())
							{
								var subOrders = contextDB.Orders.Where(p => ordersApproved.Contains(p.ID)).ToList();
								response.SubOrders = subOrders.Select(p=>new OrderDTO(p)).ToList();
							}
							
						}
						else
						{
							
							var linkOrder = contextDB.OrderRequests.Where (or=>or.SubOrderID==order.ID && or.State==(int)OrderRequestState.Approved).FirstOrDefault();
							
							if (linkOrder != null)
							{
								var parentOrder = contextDB.Orders.Where(o => o.ID == linkOrder.OrderID).FirstOrDefault();
								var routes = contextDB.OrderRoutePoints.Where(op => op.OrderID == parentOrder.ID).Select(op => new Coord() { Lat = op.Lat, Lon = op.Lon }).ToList();
								response.Routes = routes;
								if (!string.IsNullOrEmpty(parentOrder.CarID))
								{
									response.Car = contextDB.Cars.Where(c => c.ID == parentOrder.CarID).FirstOrDefault();
								}

							}
							else
							{//Список водителей если нет родителя
								var childRequest = contextDB.OrderRequests.Where(or => or.SubOrderID == order.ID && or.State == (int)OrderRequestState.Requested).Select(p => p.OrderID).ToList();
								
								if (childRequest != null && childRequest.Any())
								{
									response.Requests = contextDB.Orders.Where(p => childRequest.Contains(p.ID)).Select(p=>new OrderDTO(p)).ToList();
								}
								//var otherDriverOrders = contextDB.OrderRequests.Where(or => or.SubOrderID ==  order.ID && or.State == (int)OrderRequestState.Requested).Select(p => p.OrderID).ToList();

								var driversQuery = contextDB.Orders
									.Where(o => o.UserID != currentUser.ID && o.UserDriveStateInOrder == (int)OrderDriverStates.Driver && o.ExID != request.ExID );
								if (childRequest != null && childRequest.Any())
								{
									driversQuery = driversQuery.Where(o=>!childRequest.Contains(o.ID));
								}
								var driversOrders = driversQuery.ToList();
								response.Drivers = driversOrders.Select(o=>new OrderDTO(o)).ToList();
								foreach (var drv in response.Drivers)
								{
									drv.Routes = contextDB.OrderRoutePoints
										.Where(op => op.OrderID == drv.ID)
										.Select(op => new Coord() { Lat = op.Lat, Lon = op.Lon })
										.ToList();
									drv.Car = contextDB.Cars.Where(c => c.ID == drv.CarID).FirstOrDefault();
								}
									
							}
						}
						return Request.CreateResponse(
							HttpStatusCode.OK,
							response,
							Configuration.Formatters.JsonFormatter);
					}
					else
					{
						var message = "Такой заявки нет";
						return Request.CreateResponse(
							HttpStatusCode.NotFound,
							message,
							Configuration.Formatters.JsonFormatter);
					}
				}
				else
				{
					var message = "Такого пользователя нет, но он не зарегестрирован";
					return Request.CreateResponse(
						HttpStatusCode.NotFound,
						message,
						Configuration.Formatters.JsonFormatter);
				}
			}
		}

		[HttpPost]
		public HttpResponseMessage Post(PostOrderRequestDTO request)
		{
			var ExtUserID = request.ExtUserID;
			var ExID = request.ExID;
			var config = new HackathonWebConfig();
			using (var contextDB = HackathonORMFactory.GetContext(config))
			{
				//Check User
				var isUser = contextDB.Users.Any(u => u.ExtID == ExtUserID);
				if (isUser)
				{

					var order = contextDB.Orders.Where(o => o.ExID == ExID).FirstOrDefault();
					var currentUser = contextDB.Users.Where(u => u.ExtID == ExtUserID).First();
					contextDB.Configuration.AutoDetectChangesEnabled = false;
					//если найден ордер и текущий пользователь прописан для него, то правим
					if (order != null && order.UserID==currentUser.ID)
					{
						//изменение
						order.LatFrom = request.LatFrom;
						order.LonFrom = request.LonFrom;
						order.LatTo = request.LatTo;
						order.LonTo = request.LonTo;
						order.UserDriveStateInOrder = request.UserDriveStateInOrder;
						order.StartDateRange = (new DateTime(1970, 1, 1, 0, 0, 0, 0)).AddSeconds(request.StartDateRange);
						order.EndDateRange = (new DateTime(1970, 1, 1, 0, 0, 0, 0)).AddSeconds(request.EndDateRange);
						order.WaitLimitMin = request.WaitLimitMin;
						order.IsNeedCompanion = request.IsNeedCompanion;
						contextDB.Entry(order).State = EntityState.Modified;
					}
					else
					{//если не найден ордер или текущий пользователь не прописан для него, то вставим новый
					 //вставка
						ExID = Guid.NewGuid().ToString();
						
						var newOrder = new Order()
						{
							ExID = ExID,
							LatFrom = request.LatFrom,
							LonFrom = request.LonFrom,
							LatTo = request.LatTo,
							LonTo = request.LonTo,
							UserDriveStateInOrder = request.UserDriveStateInOrder,
							StartDateRange = (new DateTime(1970, 1, 1, 0, 0, 0, 0)).AddSeconds(request.StartDateRange),
							EndDateRange = (new DateTime(1970, 1, 1, 0, 0, 0, 0)).AddSeconds(request.EndDateRange),
							UserID = currentUser.ID,
							//StartDateRange = request.StartDateRange,
							//EndDateRange = request.EndDateRange,
							WaitLimitMin = request.WaitLimitMin,
							IsNeedCompanion = request.IsNeedCompanion,
							ID = Guid.NewGuid().ToString()
						};
						contextDB.Orders.Add(newOrder);
					}
					contextDB.ChangeTracker.DetectChanges();
					contextDB.SaveChanges();
					
					var retOrder = contextDB.Orders.Where(o => o.ExID == ExID).FirstOrDefault();

					var hereRepository = new HereRepository();
					var coord1 = new Coord()
					{
						Lat = retOrder.LatFrom,
						Lon = retOrder.LonFrom
					};
					var coord2 = new Coord()
					{
						Lat = retOrder.LatTo,
						Lon = retOrder.LonTo
					};
					var response = new OrderDTO(retOrder);
					var route = hereRepository.GetRoute(coord1, coord2);
					if (route != null && route.Points != null && route.Points.Any())
					{
						int i = 0;
						var orderPoints = new List<OrderRoutePoint>();
						foreach (var item in route.Points)
						{
							var point = new OrderRoutePoint()
							{
								ID = Guid.NewGuid().ToString(),
								OrderID = retOrder.ID,
								Sort = i++,
								Lat = item.Lat,
								Lon = item.Lon
							};
							orderPoints.Add(point);

						}
						var forDelete = contextDB.OrderRoutePoints.Where(op=>op.OrderID== retOrder.ID).ToList();
						contextDB.OrderRoutePoints.RemoveRange(forDelete);
						contextDB.OrderRoutePoints.AddRange(orderPoints);
						contextDB.ChangeTracker.DetectChanges();
						contextDB.SaveChanges();
						response.Routes = route.Points;
					}

					contextDB.Configuration.AutoDetectChangesEnabled = true;


					
					
					response.IsOwner = true;
					return Request.CreateResponse(
						HttpStatusCode.OK,
						response,
						Configuration.Formatters.JsonFormatter);
				}
				else
				{
					var message = "Такого пользователя нет, но он не зарегестрирован";
					return Request.CreateResponse(
						HttpStatusCode.NotFound,
						message,
						Configuration.Formatters.JsonFormatter);
				}
			}
		}

		[HttpGet]
		public HttpResponseMessage AcceptOrderAndRoute([FromUri]AcceptOrderAndRouteRequest request)
		{
			//save route

			return null;
		}
	}
}
