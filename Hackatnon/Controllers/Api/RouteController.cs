﻿using Hackatnon.Code.BL;
using Hackatnon.Code.Domain.Account;
using Hackatnon.Code.Domain.GIS;
using Hackatnon.Code.DTO.Cars;
using Hackatnon.Code.DTO.Routes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Hackatnon.Controllers
{
	public class RouteController : ApiController
	{
		
		[HttpGet]
		public HttpResponseMessage GetRouteForOrder([FromUri]GetRouteOrderRequest request)
		{
			var ExtUserID = request.ExtUserID;
			var orderExID = request.ExID;
			var hereRepository = new HereRepository();
			//get route
			//TODO:запрос машин в диапазоне 15 минут
			var config = new HackathonWebConfig();
			using (var contextDB = HackathonORMFactory.GetContext(config))
			{

				//Check User
				var currentUser = contextDB.Users.Where(u => u.ExtID == ExtUserID).FirstOrDefault();
				if (currentUser!=null)
				{
					//TODO:запрос машин в диапазоне 15 минут
					
					var order = contextDB.Orders.Where(o => o.ExID==orderExID && o.UserID==currentUser.ID).FirstOrDefault();
					if (order != null)
					{
						var coord1 = new Coord()
						{
							Lat = order.LatFrom,
							Lon = order.LonFrom
						};
						var coord2 = new Coord()
						{
							Lat = order.LatTo,
							Lon = order.LonTo
						};
						var route = hereRepository.GetRoute(coord1, coord2);

						return Request.CreateResponse(
							HttpStatusCode.OK,
							route,
							Configuration.Formatters.JsonFormatter);
					}
					else
					{
						var message = "Автомобилей не найдено";
						return Request.CreateResponse(
							HttpStatusCode.NotFound,
							message,
							Configuration.Formatters.JsonFormatter);
					}
				}
				else
				{
					var message = "Такого пользователя нет, но он не зарегестрирован";
					return Request.CreateResponse(
						HttpStatusCode.NotFound,
						message,
						Configuration.Formatters.JsonFormatter);
				}


			}
			
			return null;
		}
		
		
		

		// GET api/values/5
		[HttpGet]
		public HttpResponseMessage Get([FromUri]CarsRequest request)
		{
			var ExtUserID = request.ExtUserID;
			var lon = request.Lon;
			var lat = request.Lat;
			
			//TODO:запрос машин в диапазоне 15 минут
			var config = new HackathonWebConfig();
			using (var contextDB = HackathonORMFactory.GetContext(config))
			{
				//Check User
				var isUser = contextDB.Users.Any(u => u.ExtID == ExtUserID);
				if (isUser)
				{
					//TODO:запрос машин в диапазоне 15 минут
					var diff = 1.0F;
					var minLat = lat - diff;
					var maxLat = lat + diff;
					var minLon = lat - diff;
					var maxLon = lat + diff;
					var cars = contextDB.Cars.Where(o => o.Lon>=minLon && o.Lon<=maxLon && o.Lat>=minLat && o.Lat<=maxLat).ToList();
					if (cars != null && cars.Any())
					{
						var response = new CarsResponse();
						response.Cars = new List<CarListItemDTO>();
						foreach (var car in cars)
						{
							response.Cars.Add(new CarListItemDTO(car));
						}
						return Request.CreateResponse(
							HttpStatusCode.OK,
							response,
							Configuration.Formatters.JsonFormatter);
					}
					else
					{
						var message = "Автомобилей не найдено";
						return Request.CreateResponse(
							HttpStatusCode.NotFound,
							message,
							Configuration.Formatters.JsonFormatter);
					}
				}
				else
				{
					var message = "Такого пользователя нет, но он не зарегестрирован";
					return Request.CreateResponse(
						HttpStatusCode.NotFound,
						message,
						Configuration.Formatters.JsonFormatter);
				}
			}
		}
		[HttpGet]
		public HttpResponseMessage Get([FromUri]CarRequest request)
		{
			var ExtUserID = request.ExtUserID;
			var exCarID = request.ExID;

			var config = new HackathonWebConfig();
			using (var contextDB = HackathonORMFactory.GetContext(config))
			{
				//Check User
				var isUser = contextDB.Users.Any(u => u.ExtID == ExtUserID);
				if (isUser)
				{
					
					var car = contextDB.Cars.Where(o => o.ExID == exCarID).FirstOrDefault();
					if (car != null)
					{
						var response = new CarResponse(car);
						return Request.CreateResponse(
							HttpStatusCode.OK,
							response,
							Configuration.Formatters.JsonFormatter);
					}
					else
					{
						var message = "Такого автомобиля нет";
						return Request.CreateResponse(
							HttpStatusCode.NotFound,
							message,
							Configuration.Formatters.JsonFormatter);
					}
				}
				else
				{
					var message = "Такого пользователя нет, но он не зарегестрирован";
					return Request.CreateResponse(
						HttpStatusCode.NotFound,
						message,
						Configuration.Formatters.JsonFormatter);
				}
			}

		}
	


	}
}
