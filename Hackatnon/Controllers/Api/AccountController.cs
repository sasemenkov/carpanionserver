﻿using Hackatnon.Code.BL;
using Hackatnon.Code.Domain.Account;
using Hackatnon.Code.DTO.Account;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Hackatnon.Controllers
{
	public class AccountController : ApiController
	{
		// GET api/values


		// GET api/values/5
		[HttpGet]

		public HttpResponseMessage Get(string id)
		{
			var config = new HackathonWebConfig();
			var contextDB = HackathonORMFactory.GetContext(config);
			var result = contextDB.Users.Where(o => o.ExtID == id).FirstOrDefault();
			if (result != null && !string.IsNullOrEmpty(result.ID))
			{
				var userResponse = new UserResponseDTO(result);

				return Request.CreateResponse(
					HttpStatusCode.OK,
					userResponse,
					Configuration.Formatters.JsonFormatter);
			}
			else
			{
				var message = "Такого пользователя нет, но он не зарегестрирован";
				return Request.CreateResponse(
					HttpStatusCode.NotFound,
					message,
					Configuration.Formatters.JsonFormatter);

			}
		}

		//[HttpPost]

		//public HttpResponseMessage Post([FromBody]string value)
		//{
		//	var user = JsonConvert.DeserializeObject<User>(value);
		//	return Request.CreateResponse(
		//		HttpStatusCode.OK,
		//		user,
		//		Configuration.Formatters.JsonFormatter);
		//}


		[HttpPost]
		public HttpResponseMessage Register(RegisterRequestDTO registerRequest)
		//public HttpResponseMessage Register([FromBody]string value)
		{
			//var registerRequest = JsonConvert.DeserializeObject<RegisterRequestDTO>(value);
			UserResponseDTO responseUser = null;
			var config = new HackathonWebConfig();
			using (var contextDB = HackathonORMFactory.GetContext(config))
			{
				contextDB.Configuration.AutoDetectChangesEnabled = false;
				var presentUser = contextDB.Users.Where(u => u.Login == registerRequest.Login && u.PhoneNumber == registerRequest.PhoneNumber).FirstOrDefault();
				if (presentUser != null && !string.IsNullOrEmpty(presentUser.ID))
				{//пользователь уже есть
					var message = "Пользователь с такими же данными уже зарегестрирован";
					return Request.CreateResponse(
						HttpStatusCode.Forbidden,
						message,
						Configuration.Formatters.JsonFormatter);
				}
				else
				{
					try
					{
						var exUserID = Guid.NewGuid().ToString();
						var newUser = new User()
						{
							Login = registerRequest.Login,
							PhoneNumber = registerRequest.PhoneNumber,
							FirstName = registerRequest.FirstName,
							SecondName = registerRequest.SecondName,
							Sex = registerRequest.Sex,
							EMail = registerRequest.EMail,
							Password = registerRequest.Password,
							ExtID = exUserID,
							HasCarLicense = false,
							ID = Guid.NewGuid().ToString(),
							ImageUrl = null
						};
						contextDB.Users.Add(newUser);
						contextDB.ChangeTracker.DetectChanges();
						contextDB.SaveChanges();
						contextDB.Configuration.AutoDetectChangesEnabled = true;
						var insertedUser = contextDB.Users.Where(u => u.ExtID == exUserID).FirstOrDefault();
						responseUser = new UserResponseDTO(insertedUser);
					}
					catch (Exception e)
					{
						var message = e.Message;
						if (e.InnerException != null)
						{
							message += e.InnerException.Message;
						}
						return Request.CreateResponse(
							HttpStatusCode.InternalServerError,
							message,
							Configuration.Formatters.JsonFormatter);
					}


				}
			}
			if (responseUser != null)
			{
				return Request.CreateResponse(
					HttpStatusCode.Created,
					responseUser,
					Configuration.Formatters.JsonFormatter);
			}
			else
			{
				var message = "Такого пользователя нет, но он не зарегестрирован";
				return Request.CreateResponse(
					HttpStatusCode.NotFound,
					message,
					Configuration.Formatters.JsonFormatter);
			}



		}

		[HttpPost]
		public HttpResponseMessage Login(LoginRequestDTO loginRequest)
		//public HttpResponseMessage Login([FromBody]string value)
		{


			//var loginRequest = JsonConvert.DeserializeObject<LoginRequestDTO>(value);
			var config = new HackathonWebConfig();
			var contextDB = HackathonORMFactory.GetContext(config);
			var user = contextDB.Users.Where(u => 
					u.Login == loginRequest.Login 
					&& u.Password == loginRequest.Password).FirstOrDefault();
			if (user != null && !string.IsNullOrEmpty(user.ID))
			{
				var userResponse = new UserResponseDTO(user);

				return Request.CreateResponse(
					HttpStatusCode.OK,
					userResponse,
					Configuration.Formatters.JsonFormatter);
			}
			else {
				var message = "Неудачная авторизация";
				return Request.CreateResponse(
					HttpStatusCode.Unauthorized,
					message,
					Configuration.Formatters.JsonFormatter);

			}
		}
		//[HttpPost]

		//public HttpResponseMessage SetCarSheringList([FromBody]string value)
		//{
		//	var user = JsonConvert.DeserializeObject<User>(value);
		//	return Request.CreateResponse(
		//		HttpStatusCode.OK,
		//		user,
		//		Configuration.Formatters.JsonFormatter);
		//}
	}
}
