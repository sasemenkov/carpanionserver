﻿using Hackatnon.Code.BL;
using Hackatnon.Code.Domain.Account;
using Hackatnon.Code.DTO.Cars;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Hackatnon.Controllers
{
	public class CarSheringController : ApiController
	{
		// GET api/values
		

		// GET api/values/5
		[HttpGet]

		public HttpResponseMessage Get(string ExtUserID)
		{
			var config = new HackathonWebConfig();
			using (var contextDB = HackathonORMFactory.GetContext(config))
			{
				//Check User
				var isUser = contextDB.Users.Any(u => u.ExtID == ExtUserID);
				if (isUser)
				{

					var carSherings = contextDB.CarSherings.ToList();
					if (carSherings != null && carSherings.Any())
					{
						var response = new CarSheringsResponse();
						response.CarSherings = new List<CarSheringDTO>();
						foreach (var carShering in carSherings)
						{
							response.CarSherings.Add(new CarSheringDTO(carShering));
						}
						return Request.CreateResponse(
							HttpStatusCode.OK,
							response,
							Configuration.Formatters.JsonFormatter);
					}
					else
					{
						var message = "Доступных каршерингов в системене не обнаружено";
						return Request.CreateResponse(
							HttpStatusCode.NotFound,
							message,
							Configuration.Formatters.JsonFormatter);
					}
				}
				else
				{
					var message = "Такого пользователя нет, но он не зарегестрирован";
					return Request.CreateResponse(
						HttpStatusCode.NotFound,
						message,
						Configuration.Formatters.JsonFormatter);
				}
			}
		}

		[HttpPost]

		public HttpResponseMessage Post([FromBody]string value)
		{
			var user = JsonConvert.DeserializeObject<User>(value);
			return Request.CreateResponse(
				HttpStatusCode.OK,
				user,
				Configuration.Formatters.JsonFormatter);
		}

		
	}
}
